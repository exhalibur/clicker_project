window.onload = function () {
    deleteAllCookies();
    console.log(document.cookie)
    var cookies = document.cookie.split(";");
    var value_old = cookies[0];
    console.log(value_old)
    if (value_old){
        var found_value = /\d+/.exec(value_old)[0];
        old_score = parseInt(found_value);
    }
    else{
        old_score = 0;
    } 
    var builds = document.createElement('img');
    builds.src = 'images/background.png';
    builds.addEventListener('load', Loadbuilds, false);
    function Loadbuilds(){
        builds.setAttribute('width', '100%');
        builds.setAttribute('height', '100%');
        builds.style.bottom = '0';
        builds.style.position = 'absolute';
        builds.style.maxHeight = '25%';
        
        builds = document.body.appendChild(builds);
    }
    let fish_div = document.createElement('div');
    fish_div.style.position = 'fixed';
    fish_div.style.left = '50%';
    fish_div.style.transform = "translateX(-50%)";
    
   var score = old_score;
    let fish = document.createElement('img');
    fish.src = 'images/click-object.png';
    fish.addEventListener('load', LoadFish, false);
    function LoadFish(){
        fish.setAttribute('width', '500px');
        fish.setAttribute('height', '500px');
        fish.setAttribute('alt', 'img2');
        fish.setAttribute('id', 'img2');
        fish.setAttribute('z-index', '110');
        fish.style.display = 'block';
        fish.style.marginLeft = 'auto';
        fish.style.marginRight = 'auto';
        fish.addEventListener('mousedown', addCountUp, false);
        fish.addEventListener('mouseup', addCountDown, false);
        function addCountUp(){
            score += 1;
            scoreAdd(score);
            fish.style.width = '480px';
            fish.style.height = '480px';

        }
        function addCountDown(){
            fish.style.width = '500px';
            fish.style.height = '500px';
            deleteAllCookies();
            set_cookie('value', score);
            
        }
        fish_div.appendChild(fish);
        document.body.appendChild(fish_div);

        var score_h = document.createElement("H1");
        score_h.id = "sc_text";
        score_h.textContent = "Score: "+ score;
            score_h = document.body.appendChild(score_h);
        function scoreAdd(score){
            score_h.textContent = "Score: "+ score;
        }
        score_h.style.position = 'fixed';
        score_h.style.top = '40%';
        score_h.style.left = '45%';

    }
    
    function set_cookie( name, value, expires, path, domain, secure ) {
        var cookie_string = name + "=" + escape ( value );
        if ( typeof expires == 'number' && expires > 0 ) {
            var d = new Date();
            var n = new Date(d.setTime( d.getTime() + expires*1000 ));
            expires = n.toUTCString();
            cookie_string += "; expires=" + expires;
        }
        if ( path ){
            cookie_string += "; path=" + escape ( path );
        }
        else {
            cookie_string += "; path=/";
        }
        if ( domain )
            cookie_string += "; domain=" + escape ( domain );
        if ( secure )
            cookie_string += "; secure";
        document.cookie = cookie_string;
    }
    function deleteAllCookies() {
        var cookies = document.cookie.split(";");
    
        for (var i = 0; i < cookies.length; i++) {
            var cookie = cookies[i];
            var eqPos = cookie.indexOf("=");
            var name = eqPos > -1 ? cookie.substr(0, eqPos) : cookie;
            document.cookie = name + "=;expires=Thu, 01 Jan 1970 00:00:00 GMT";
        }
    }
}
    
